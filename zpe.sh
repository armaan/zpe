#!/usr/bin/env bash
# ZPE shell plugin helper
# Copyright 2021 Armaan Bhojwani, MIT License

# Set defaults
zpe_config_dir="${HOME}/.config/zpe/"
zpe_plugin_dir="${zpe_config_dir}/plugins/"
zpe_tmp_dir="/tmp/zpe/"
zpe_log_dir="${zpe_tmp_dir}/logs"

zpe-meta-pre() {
  # Create necesary directories, check dependencies
  [[ -d "${zpe_config_dir}" ]] || mkdir -p "${zpe_config_dir}"
  [[ -d "${zpe_plugin_dir}" ]] || mkdir -p "${zpe_plugin_dir}"
  [[ -d "${zpe_tmp_dir}" ]] || mkdir ${zpe_tmp_dir}
  [[ -d "${zpe_log_dir}" ]] || mkdir ${zpe_log_dir}
  touch "${zpe_config_dir}"repositories
  [[ ! -x "/usr/bin/git" ]] && echo "Please install git"
  log_file="${zpe_log_dir}"/$(date -Iseconds)
  touch $log_file
}

zpe-clone() {
  # Clone all the repos in the config file recursively into the plugin directory
  zpe-meta-pre
  cat ${zpe_config_dir}repositories | xargs -P10 -I{} git -C ${zpe_plugin_dir} \
    clone {} -q --depth 1 &> "${zpe_log_dir}"/$(date -Iseconds)
  echo "All plugins are downloaded"
}

zpe-pull() {
  # Recursively pull
  zpe-meta-pre
  local find_dirs=$(find "${zpe_plugin_dir}" -name ".git" -type d)
  echo $find_dirs | xargs -P10 -I {} git --git-dir={} config pull.ff only
  echo $find_dirs | xargs -P10 -I {} git --git-dir={} --work-tree={} pull -v \
    &> $log_file
  echo >> $log_file
  cat $log_file
  echo "All plugins are up to date"
}

zpe-clean() {
  # Remove cloned plugins not specified in the config file
  zpe-meta-pre
  find ${zpe_plugin_dir} -maxdepth 1 -type d -exec git -C {} \
    config --get remote.origin.url \; > "${zpe_tmp_dir}"installed-urls
  comm -23 <(sort ${zpe_tmp_dir}installed-urls) \
    <(sort "${zpe_config_dir}"repositories) > "${zpe_tmp_dir}"/diff

  echo -n "" > "${zpe_tmp_dir}"deletable
  while read line; do
    grep -l $line "$zpe_plugin_dir"/*/.git/config >> "${zpe_tmp_dir}"deletable
  done < ${zpe_tmp_dir}diff

  sed -i -e 's/\/.git\/config//g' -e 's/\/\//\//g' "${zpe_tmp_dir}"deletable

  if [[ $(wc -l "${zpe_tmp_dir}"deletable | cut -d ' ' -f 1) > 0 ]]; then
    cat "${zpe_tmp_dir}"deletable
    echo "Delete these directories? [y/N]: "
    read -rs -k 1 ans
    case "${ans}" in
      y|Y)
        while read line2; do
          xargs rm -rf <<< $(echo $line2)
        done < ${zpe_tmp_dir}deletable
        echo "Cleaned!"
        ;;
      *)
        echo "Aborted"
    esac
  else
    echo "Nothing to clean"
  fi
}

zpe-source() {
  source "${zpe_plugin_dir}"/$1
}

zpe() {
  ${EDITOR} ${zpe_config_dir}repositories
}
